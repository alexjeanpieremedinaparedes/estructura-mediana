import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//--------------------------------------------------------------------------------------- Views

// Admin
import { VentasComponent } from './admin/ventas/ventas.component';
import { ReportesComponent } from './admin/reportes/reportes.component';
import { InicioadminComponent } from './admin/inicioadmin/inicioadmin.component';

// Web
import { IniciowebComponent } from './web/inicioweb/inicioweb.component';
import { NosotrosComponent } from './web/nosotros/nosotros.component';
import { ContactosComponent } from './web/contactos/contactos.component';


@NgModule({

  declarations: [
    
    // Admin
    VentasComponent,
    ReportesComponent,
    InicioadminComponent,

    // Web
    ContactosComponent,
    IniciowebComponent,
    NosotrosComponent,
  ],

  imports: [
    CommonModule
  ],

  exports:[
    VentasComponent,
    ReportesComponent,
    InicioadminComponent,
    IniciowebComponent,
    NosotrosComponent,
    ContactosComponent
  ]
})
export class ViewModule { }
