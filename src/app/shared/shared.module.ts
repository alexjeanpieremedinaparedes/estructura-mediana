import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarwebComponent } from './navbarweb/navbarweb.component';
import { FooterwebComponent } from './footerweb/footerweb.component';
import { NavbaradminComponent } from './navbaradmin/navbaradmin.component';
import { FooteradminComponent } from './footeradmin/footeradmin.component';
import { RouterModule } from '@angular/router';
import { ViewModule } from '../view/view.module';
import { AppRoutingModule } from '../app-routing.module';

@NgModule({
  declarations: [
    NavbarwebComponent,
    FooterwebComponent,
    NavbaradminComponent,
    FooteradminComponent
  ],

  imports: [
    CommonModule,
    AppRoutingModule,
    RouterModule,
    ViewModule
  ],

  exports:[
    NavbarwebComponent,
    FooterwebComponent,
    NavbaradminComponent,
    FooteradminComponent
  ]
  
})
export class SharedModule { }
