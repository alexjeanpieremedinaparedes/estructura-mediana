import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AdminestrucinicialComponent } from '../layouts/admin/adminestrucinicial/adminestrucinicial.component';
import { InicioadminComponent } from './admin/inicioadmin/inicioadmin.component';
import { ReportesComponent } from './admin/reportes/reportes.component';
import { VentasComponent } from './admin/ventas/ventas.component';
import { WebestrucinicialComponent } from '../layouts/web/webestrucinicial/webestrucinicial.component';
import { ContactosComponent } from './web/contactos/contactos.component';
import { NosotrosComponent } from './web/nosotros/nosotros.component';
import { IniciowebComponent } from './web/inicioweb/inicioweb.component';


const routes: Routes = [
    
    {path:'admin',component:AdminestrucinicialComponent,
        children:[
            {path:'inicioadmin',component:InicioadminComponent},
            {path:'reportes',component:ReportesComponent},
            {path:'ventas', component:VentasComponent},
            {path:'', pathMatch:'full' , redirectTo:'inicioadmin'}
        ]
    },

    {path:'web',component:WebestrucinicialComponent,
        children:[
            {path:'inicioweb',component:IniciowebComponent},
            {path:'contactos',component:ContactosComponent},
            {path:'nosotros', component:NosotrosComponent},
            {path:'', pathMatch:'full' , redirectTo:'inicioweb'}
        ]
    }

];

@NgModule({
    imports: [RouterModule.forChild(routes),],
    exports: [RouterModule]
})
export class ViewRoutingModule {}
