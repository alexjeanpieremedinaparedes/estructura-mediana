import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//------------------------------------------------------------------------------------------- Layouts

// Admin
import { ConfiguracionesComponent } from './admin/configuraciones/configuraciones.component';
import { AdminestrucinicialComponent } from './admin/adminestrucinicial/adminestrucinicial.component';


// inicialweb
import { WebestrucinicialComponent } from './web/webestrucinicial/webestrucinicial.component';
import { OtroComponent } from './web/otro/otro.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from '../app-routing.module';
import { ViewModule } from '../view/view.module';



@NgModule({
  declarations: [
    AdminestrucinicialComponent,
    ConfiguracionesComponent,
    WebestrucinicialComponent,
    OtroComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    AppRoutingModule,
    ViewModule,
    
  ],
  exports:[
    AdminestrucinicialComponent,
    ConfiguracionesComponent,
    WebestrucinicialComponent,
    OtroComponent
  ]
})
export class LayoutsModule { }
