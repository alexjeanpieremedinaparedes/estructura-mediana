import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './auth/pages/login/login.component';
import { ViewRoutingModule } from './view/view.routes';

const routes: Routes = [
  {path:'login', component:LoginComponent},
  {path:'**', pathMatch:'full' ,redirectTo:'login' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash:true}),
            ViewRoutingModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
